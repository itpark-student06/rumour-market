package ru.itpark.rumourmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itpark.rumourmarket.entity.Rumour;

public interface RumourRepository extends JpaRepository<Rumour, Integer> {

}
