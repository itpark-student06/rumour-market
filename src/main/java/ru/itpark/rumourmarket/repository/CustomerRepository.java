package ru.itpark.rumourmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itpark.rumourmarket.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    Customer findByFirstName(String name);
}
