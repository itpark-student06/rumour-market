package ru.itpark.rumourmarket.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.itpark.rumourmarket.dto.RumourDTO;
import ru.itpark.rumourmarket.entity.Rumour;
import ru.itpark.rumourmarket.exception.*;
import ru.itpark.rumourmarket.repository.RumourRepository;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class RumourService {
    private final RumourRepository rumourRepository;
    private final CustomerService customerService;
    private final Path mediaPath;

    public RumourService(RumourRepository repository, CustomerService customerService, @Value("${app.media-path}") String mediaPath) {
        this.rumourRepository = repository;
        this.customerService = customerService;
        this.mediaPath = Paths.get(mediaPath);
    }

    public Stream<RumourDTO> findAll() {

        return rumourRepository.findAll()
                .stream()
                .map(e -> new RumourDTO(
                        e.getRumourId(),
                        e.getName(),
                        e.getDescription(),
                        e.getPrice(),
                        e.getPayload(),
                        e.getCustomer()
                ));
    }

    public void saveRumour(RumourDTO dto, MultipartFile file) {

        if (dto.getRumourId() == 0) {
            var entity = new Rumour(dto.getRumourId(), dto.getName(), dto.getDescription(), dto.getPrice(), getPayloadName(file), dto.getCustomer());
            rumourRepository.save(entity);

            return;
        }

        var entity = rumourRepository.getOne(dto.getRumourId());

        entity.setName(dto.getName());
        entity.setPrice(dto.getPrice());
        entity.setDescription(dto.getDescription());

        if (file.isEmpty()) {
            rumourRepository.save(entity);

            return;
        }

        Path target = mediaPath.resolve(entity.getPayload());
        try {
            Files.deleteIfExists(target);
        } catch (IOException e) {
            throw new MediaRemoveException("The media doesn't exist", e);
        }

        entity.setPayload(getPayloadName(file));
        rumourRepository.save(entity);
    }

    private String getPayloadName(MultipartFile file) {
        var contentType = file.getContentType();

        String name = UUID.randomUUID().toString();

        String ext = getExt(contentType);

        Path target = mediaPath.resolve(name + ext);

        try {
            file.transferTo(target.toFile());
        } catch (IOException e) {
            throw new MediaUploadException(e);
        }

        return name + ext;
    }

    private String getExt(String contentType) {
        String ext;
        if (MediaType.APPLICATION_PDF_VALUE.equals(contentType)) {
            ext = ".pdf";
        } else {
            throw new UnsupportedMediaTypeException(contentType);
        }
        return ext;
    }

    public void deleteById(int id) {

        var entity = rumourRepository.findById(id).orElseThrow(() -> new RumourNotFoundException("The rumour with that customerId is doesn't exist"));

        Path target = mediaPath.resolve(entity.getPayload());

        try {
            Files.deleteIfExists(target);
        } catch (IOException e) {
            throw new MediaRemoveException("The media doesn't exist", e);
        }

        rumourRepository.delete(entity);
    }

    public Resource loadFileAsResource(int id) {
        String fileName = rumourRepository.getOne(id).getPayload();
        Path path = this.mediaPath.resolve(fileName).normalize();
        try {
            Resource resource = new UrlResource(path.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException e) {
            throw new MyFileNotFoundException("File not found " + fileName, e);
        }
    }

    public ResponseEntity<Resource> downloadPayload(int id, HttpServletRequest request, String buyer) {
        Resource resource = loadFileAsResource(id);

        var entity = rumourRepository.findById(id).orElseThrow(() -> new RumourNotFoundException("The rumour with that customerId is doesn't exist"));

        entity.setCustomer(customerService.findByFirstName(buyer));

        rumourRepository.save(entity);

        String contentType;

        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException e) {
            throw new UnsupportedMediaTypeException("Could not determine file type.");
        }

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
