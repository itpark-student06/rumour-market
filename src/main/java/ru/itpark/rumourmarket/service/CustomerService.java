package ru.itpark.rumourmarket.service;

import org.springframework.stereotype.Service;
import ru.itpark.rumourmarket.dto.CustomerDTO;
import ru.itpark.rumourmarket.entity.Customer;
import ru.itpark.rumourmarket.exception.CustomerNotFoundException;
import ru.itpark.rumourmarket.repository.CustomerRepository;
import ru.itpark.rumourmarket.repository.RumourRepository;

import java.util.stream.Stream;

@Service
public class CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository, RumourRepository rumourRepository, RumourRepository rumourRepository1, RumourRepository rumourRepository2) {
        this.customerRepository = customerRepository;
    }

    public Stream<CustomerDTO> findAll() {
        return customerRepository.findAll()
                .stream()
                .map(e -> new CustomerDTO(
                        e.getCustomerId(),
                        e.getFirstName(),
                        e.getSecondName(),
                        e.getEmail(),
                        e.getRumours()
                ));
    }

    public void deleteById(int id) {

        var entity = customerRepository.findById(id).orElseThrow(() -> new CustomerNotFoundException("The client with that customerId is doesn't exist"));
        customerRepository.delete(entity);
    }

    public void saveCustomer(CustomerDTO dto) {

        if (dto.getCustomerId() == 0) {
            var entity = new Customer(dto.getCustomerId(), dto.getFirstName(), dto.getSecondName(), dto.getEmail(), dto.getRumours());
            customerRepository.save(entity);

            return;
        }

        var entity = customerRepository.findById(dto.getCustomerId()).orElseThrow(() -> new CustomerNotFoundException("The client with that customerId is doesn't exist"));

        entity.setFirstName(dto.getFirstName());
        entity.setSecondName(dto.getSecondName());
        entity.setEmail(dto.getEmail());

        customerRepository.save(entity);
    }

    public Customer findByFirstName(String firstName) {

        return  customerRepository.findByFirstName(firstName);
    }

}
