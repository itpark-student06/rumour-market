package ru.itpark.rumourmarket.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Rumour {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int rumourId;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String description;
    @Column(nullable = false)
    private int price;
    @Column(nullable = false)
    private String payload;
    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="customer_id")
    private Customer customer;
}
