package ru.itpark.rumourmarket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itpark.rumourmarket.entity.Customer;

import javax.validation.constraints.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RumourDTO {
    private int rumourId;
    @NotBlank
    @Size(min = 5, max = 50)
    private String name;
    @NotBlank
    @Size(min = 5, max = 100)
    private String description;
    @NotNull
    @Positive
    private int price;
    @NotNull
    private String payload;
    private Customer customer;
}
