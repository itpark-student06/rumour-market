package ru.itpark.rumourmarket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itpark.rumourmarket.entity.Rumour;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDTO {
    private int customerId;
    @NotBlank
    @Size(min = 5, max = 50)
    private String firstName;
    @NotBlank
    @Size(min = 5, max = 50)
    private String secondName;
    @NotBlank
    @Size(min = 5, max = 50)
    private String email;
    private List<Rumour> rumours;
}
