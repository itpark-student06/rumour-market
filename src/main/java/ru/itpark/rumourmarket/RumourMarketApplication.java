package ru.itpark.rumourmarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.itpark.rumourmarket.entity.Customer;
import ru.itpark.rumourmarket.entity.Rumour;
import ru.itpark.rumourmarket.repository.CustomerRepository;
import ru.itpark.rumourmarket.repository.RumourRepository;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class RumourMarketApplication {

    public static void main(String[] args) {

        var context = SpringApplication.run(RumourMarketApplication.class, args);

        var clientRepository = context.getBean(CustomerRepository.class);
        clientRepository.saveAll(List.of(
                new Customer(0, "Vasya", "Ivanov", "vasya93@hotmail.com", new ArrayList<>()),
                new Customer(0, "Petya", "Sidorov", "petya88@hotmail.com", new ArrayList<>()),
                new Customer(0, "Ivan", "Petrov", "ivan92@hotmail.com", new ArrayList<>())
        ));

        var rumourRepository = context.getBean(RumourRepository.class);
        rumourRepository.saveAll(List.of(
                new Rumour(0, "Kazan rumour", "Information on the demolition of residential buildings in the city of Kazan over the next four years",  100_000, "Project1.pdf", null),
                new Rumour(0, "Journalistic investigation", "Cristiano Ronaldo rape allegation: lawyers' materials", 200_000, "Project2.pdf", null),
                new Rumour(0, "Fair competition", "Magnit modernization plan over five yeas", 500_000, "Project3.pdf", null)
        ));





    }
}
