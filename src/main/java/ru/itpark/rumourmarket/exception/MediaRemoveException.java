package ru.itpark.rumourmarket.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MediaRemoveException extends RuntimeException {
    public MediaRemoveException() {
    }

    public MediaRemoveException(String message) {
        super(message);
    }

    public MediaRemoveException(String message, Throwable cause) {
        super(message, cause);
    }

    public MediaRemoveException(Throwable cause) {
        super(cause);
    }

    public MediaRemoveException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
