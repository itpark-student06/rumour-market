package ru.itpark.rumourmarket.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class RumourNotFoundException extends RuntimeException {
    public RumourNotFoundException() {
    }

    public RumourNotFoundException(String message) {
        super(message);
    }

    public RumourNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public RumourNotFoundException(Throwable cause) {
        super(cause);
    }

    public RumourNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
