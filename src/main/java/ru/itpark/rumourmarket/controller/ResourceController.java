package ru.itpark.rumourmarket.controller;

import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.itpark.rumourmarket.dto.CustomerDTO;
import ru.itpark.rumourmarket.dto.RumourDTO;
import ru.itpark.rumourmarket.service.CustomerService;
import ru.itpark.rumourmarket.service.RumourService;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Stream;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ResourceController {
    private final RumourService rumourService;
    private final CustomerService customerService;

    public ResourceController(RumourService service, CustomerService customerService) {
        this.rumourService = service;
        this.customerService = customerService;
    }

    @GetMapping("/customers")
    public Stream<CustomerDTO> getAllCustomers() {
        return customerService.findAll();
    }

    @PostMapping("/customers/add")
    public void addCustomer(@RequestBody CustomerDTO customerDTO) {
        customerService.saveCustomer(customerDTO);
    }

    @DeleteMapping("/customers/{customerId}")
    public void deleteCustomerById(@PathVariable int customerId) {
        customerService.deleteById(customerId);
    }

    @GetMapping("/rumours")
    public Stream<RumourDTO> getAllRumours() {
        return rumourService.findAll();
    }

    @PostMapping(value = "/rumours/add", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void addRumour(@RequestPart RumourDTO dto, @RequestPart MultipartFile file) {
        rumourService.saveRumour(dto, file);
    }

    @GetMapping("/rumours/{rumourId}/download")
    public ResponseEntity<Resource> downloadPayloadById(@PathVariable int rumourId, HttpServletRequest request, String buyer){
        return rumourService.downloadPayload(rumourId, request, buyer);
    }

    @DeleteMapping("/rumours/{rumourId}")
    public void deleteRumourById(@PathVariable int rumourId) {
        rumourService.deleteById(rumourId);
    }
}
