// DOM -> Document Object Model
class Rumour {
    constructor(name, description, price, payload) {
        this.rumourId = 0;
        this.name = name;
        this.description = description;
        this.price = price;
        this.payload = payload;
        this.customers = [];
    }
}

class Customer {
    constructor(firstName, secondName, email) {
        this.customerId = 0;
        this.firstName = firstName;
        this.secondName = secondName;
        this.email = email;
        this.rumours = [];
    }
}


// DOM - Document Object Model
const apiUrl = 'http://localhost:8080/api';

const rumourNameInput = document.getElementById('rumourName');
const rumourDescriptionInput = document.getElementById('rumourDescription');
const rumourPriceInput = document.getElementById('rumourPrice');
const rumourPayloadInput = document.getElementById('rumourPayload');
const rumourAddButton = document.getElementById('rumourAdd');
const rumourCardColumns = document.getElementById('rumourCardColumns');

const customerFirstNameInput = document.getElementById('customerFirstName');
const customerSecondNameInput = document.getElementById('customerSecondName');
const customerEmailInput = document.getElementById('customerEmail');
const customerAddButton = document.getElementById('customerAdd');
const customersListUl = document.getElementById('customerList');

const rumours = [];
const customers = [];// vue, angular, react -> синхронизация памяти и отображения

loadRumours(rumours);
loadCustomers(customers);

rumourAddButton.addEventListener('click', e => { // e - объект события
    // выполняй функцию, когда произойдёт событие

    // просим не выполнять действие по умолчанию
    e.preventDefault(); // e -> объект события

    const valueOfName = rumourNameInput.value.trim(); // убрать начальные и концевые пробелы
    if (valueOfName === '') { // ==
        alert('Please input the rumour name');
        // noinspection JSAnnotator
        return;
    }

    const valueOfDescription = rumourDescriptionInput.value.trim(); // убрать начальные и концевые пробелы
    if (valueOfDescription === '') { // ==
        alert('Please input the rumour description');
        return;
    }

    const valueOfPrice = rumourPriceInput.value.trim(); // убрать начальные и концевые пробелы
    if (valueOfDescription === '') { // ==
        alert('Please input the rumour price');
        return;
    }

    const valueOfPayload = rumourPayloadInput.files[0];
    if (valueOfPayload === '') {
        alert('Please choose the rumour payload file');
        return;
    }

    const rumour = new Rumour(valueOfName, valueOfDescription, valueOfPrice);
    createRumour(rumour, valueOfPayload);

    rumourNameInput.value = '';
    rumourDescriptionInput.value = '';
    rumourPriceInput.value = '';
    rumourPayloadInput.value = '';
});


customerAddButton.addEventListener('click', e => { // e - объект события
    // выполняй функцию, когда произойдёт событие

    // просим не выполнять действие по умолчанию
    e.preventDefault(); // e -> объект события

    const valueOfFirstName = customerFirstNameInput.value.trim(); // убрать начальные и концевые пробелы
    if (valueOfFirstName === '') { // ==
        alert('Please input the customer first name');
        return;
    }

    const valueOfSecondName = customerSecondNameInput.value.trim(); // убрать начальные и концевые пробелы
    if (valueOfSecondName === '') { // ==
        alert('Please input the customer second name');
        return;
    }

    const valueOfEmail = customerEmailInput.value.trim(); // убрать начальные и концевые пробелы
    if (valueOfEmail === '') { // ==
        alert('Please input the customer e-mail');
        return;
    }

    const customer = new Customer(valueOfFirstName, valueOfSecondName, valueOfEmail);
    createCustomer(customer);

    customerFirstNameInput.value = '';
    customerSecondNameInput.value = '';
    customerEmailInput.value = '';
});

function renderRumours(rumours) {
    rumourCardColumns.innerHTML = ''; // bad hack
    Array.from(rumourCardColumns.children).forEach(e => {
        rumourCardColumns.removeChild(e);
    });

    rumours.forEach(el => {
        const divCard = document.createElement('div');
        divCard.classList.add('card');
        divCard.classList.add('text-white');
        divCard.classList.add('bg-info');
        divCard.classList.add('mb-3');
        divCard.style.setProperty('max-width', '18rem');

        const cardHeader = document.createElement('div');
        cardHeader.classList.add('card-header');
        cardHeader.innerHTML = `${el.name}`;
        divCard.appendChild(cardHeader);

        const divCardBody = document.createElement('div');
        divCardBody.classList.add('card-body');
        divCard.appendChild(divCardBody);

        const cardTitle = document.createElement('h5');
        cardTitle.classList.add('card-title');
        cardTitle.innerHTML = `${el.description}`;
        divCardBody.appendChild(cardTitle);

        const cardText = document.createElement('p');
        cardText.classList.add('card-text');
        cardText.innerText = `${el.price}`;
        cardText.innerHTML = `
            <button class="rumour-remove btn btn-danger btn-sm float-right">Remove</button>
            <button class="rumour-buy btn btn-primary btn-sm float-right">Buy</button>
        `;
        divCardBody.appendChild(cardText);

        const btnRumourRemove = divCard.querySelector('.rumour-remove');
        btnRumourRemove.addEventListener('click', e => {
            removeRumourById(el.rumourId);
        });

        const btnBuy = divCard.querySelector('.rumour-buy');
        btnBuy.addEventListener('click', e => {
            buyRumourById(el.rumourId);
        });

        // if (el.done === true) {
        //     card.classList.add('rumour-done'); // без точки - это имя класса
        // }


        // li.addEventListener('click', e => {
        //     // target - тот, на ком реально кликнули
        //     // currentTarget - тот, в обработчике события которого мы находимся
        //     if (e.target === e.currentTarget) {
        //         toggleRumourDone(el.id, el.done);
        //     }
        // });

        // css-selectors:
        // #id -> selector by id
        // .classname -> selector by class
        // tag -> selector by tag

        rumourCardColumns.appendChild(divCard);
    });
}

function renderCustomers(customers) {
    customersListUl.innerHTML = ''; // bad hack
    // Array.from(taskListUl.children).forEach(e => {
    //    taskListUl.removeChild(e);
    // });

    customers.forEach(el => {
        const li = document.createElement('li');
        // const btn = document.createElement('button');
        // btn.classList.add('btn', 'btn-danger', 'btn-sm', 'float-right');
        // btn.textContent = 'remove';
        //
        // li.appendChild(btn);

        li.innerHTML = `
            ${el.firstName}
            <button class="customer-remove btn btn-danger btn-sm float-right">Remove</button>
        `;

        li.classList.add('list-group-item');

        // css-selectors:
        // #id -> selector by id
        // .classname -> selector by class
        // tag -> selector by tag
        const btnCustomerRemove = li.querySelector('.customer-remove');
        btnCustomerRemove.addEventListener('click', e => {
            removeCustomerById(el.customerId);
        });

        customersListUl.appendChild(li);
    });
}

function loadRumours(rumours) {
    // XMLHttpRequest
    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', e => {
        // JSON - текстовый формат
        // TODO: нужно проверять статус ответа
        const data = JSON.parse(xhr.responseText);
        // data - массив task'ов
        rumours.splice(0, rumours.length);
        // tasks.length = 0;
        rumours.push(...data); // ...распаковка
        renderRumours(rumours);
    });
    xhr.addEventListener('error', e => {
        console.log(e);
    });
    xhr.open('GET', `${apiUrl}/rumours`); // backtick - вклеивание значения переменной в строку
    xhr.setRequestHeader("Authorization", "Basic " + btoa("admin1:secret1"));
    xhr.send();
}

function loadCustomers(customers) {
    // XMLHttpRequest
    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', e => {
        // JSON - текстовый формат
        // TODO: нужно проверять статус ответа
        const data = JSON.parse(xhr.responseText);
        // data - массив task'ов
        customers.splice(0, customers.length);
        // tasks.length = 0;
        customers.push(...data); // ...распаковка
        renderCustomers(customers);
    });
    xhr.addEventListener('error', e => {
        console.log(e);
    });
    xhr.open('GET', `${apiUrl}/customers`); // backtick - вклеивание значения переменной в строку
    xhr.setRequestHeader("Authorization", "Basic " + btoa("admin1:secret1"));
    xhr.send();
}

function createRumour(rumour, file) {
    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', e => {
        // tasks.push(task); - менее нагружает сервер
        // renderRumours(tasks);
        // TODO: xhr.responseStatus
        loadRumours(rumours);
    });
    xhr.addEventListener('error', e => {
        console.log(e);
    });

    const formData = new FormData();
    formData.append("dto", new Blob([JSON.stringify(rumour)], {type: 'application/json'}));
    formData.append("file", file);

    xhr.open('POST', `${apiUrl}/rumours/add`); // backtick - вклеивание значения переменной в строку
    xhr.setRequestHeader("Authorization", "Basic " + btoa("admin1:secret1"));
    xhr.send(formData); // stringify: object -> string
}

function createCustomer(customer) {
    const xh = new XMLHttpRequest();
    xh.addEventListener('load', e => {
        // tasks.push(task); - менее нагружает сервер
        // renderTasks(tasks);
        // TODO: xhr.responseStatus
        loadCustomers(customers);
    });
    xh.addEventListener('error', e => {
        console.log(e);
    });
    xh.open('POST', `${apiUrl}/customers/add`); // backtick - вклеивание значения переменной в строку
    xh.setRequestHeader('Content-Type', 'application/json');
    xh.setRequestHeader("Authorization", "Basic " + btoa("admin1:secret1"));
    xh.send(JSON.stringify(customer)); // stringify: object -> string
}

function removeRumourById(rumourId) {
    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', e => {
        // tasks.push(task); - менее нагружает сервер
        // renderRumours(tasks);
        // TODO: xhr.responseStatus
        loadRumours(rumours);
    });
    xhr.addEventListener('error', e => {
        console.log(e);
    });
    xhr.open('DELETE', `${apiUrl}/rumours/${rumourId}`); // backtick - вклеивание значения переменной в строку
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader("Authorization", "Basic " + btoa("admin1:secret1"));
    xhr.send();
}

function removeCustomerById(customerId) {
    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', e => {
        // tasks.push(task); - менее нагружает сервер
        // renderRumours(tasks);
        // TODO: xhr.responseStatus
        loadCustomers(customers);
    });
    xhr.addEventListener('error', e => {
        console.log(e);
    });
    xhr.open('DELETE', `${apiUrl}/customers/${customerId}`); // backtick - вклеивание значения переменной в строку
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader("Authorization", "Basic " + btoa("admin1:secret1"));
    xhr.send();
}

function buyRumourById(rumourId) {

    const xhr = new XMLHttpRequest();
    let rumour;

    rumours.forEach(el => {
        if (el.rumourId === rumourId)
            rumour = el;
    });

    xhr.responseType = "blob";
    xhr.addEventListener('load', e => {
        // tasks.push(task); - менее нагружает сервер
        // renderRumours(tasks);
        // TODO: xhr.responseStatus
        const ghost = document.createElement('a');
        const objectUrl = URL.createObjectURL(xhr.response);
        ghost.href = objectUrl;
        ghost.download = `${rumour.payload}`;

        document.body.appendChild(ghost);
        ghost.dispatchEvent(new MouseEvent('click', {}));

        setTimeout(() => {
            URL.revokeObjectURL(objectUrl);
            document.body.removeChild(ghost);
        }, 60 * 1000);

    });

    xhr.open('GET', `${apiUrl}/rumours/${rumourId}/download`); // backtick - вклеивание значения переменной в строку
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader("Authorization", "Basic " + btoa("admin1:secret1"));
    xhr.send();
}